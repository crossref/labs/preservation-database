# Changelog

## 0.0.83: 2023-04-03
* Add function to normalize JSON keys

## 0.0.82: 2023-03-14
* Normalize JSON keys

## 0.0.77: 2023-03-06
* Display tweaks

## 0.0.76: 2023-03-06
* Update sample used to most recent
* Export DOI preservation information to S3
* Stash list of unpreserved items

## 0.0.74: 2023-03-05
* Fix loop bug in rescore

## 0.0.73: 2023-03-03
* More groupings in report export
* Externalize streamlit reports

## 0.0.68: 2023-03-03
* Add rescoring mechanism
* Fix scoring

## 0.0.52: 2023-02-28
* Internet Archive item-level lookup
* Initial reporting work

## 0.0.51: 2023-02-28
* Fix buffering

## 0.0.50: 2023-02-28
* More logging

## 0.0.49: 2023-02-28
* More logging

## 0.0.48: 2023-02-28
* Fix logging

## 0.0.47: 2023-02-28
* Add logging

## 0.0.46: 2023-02-28
* Fix requirements

## 0.0.45: 2023-02-28
* Performant internet archive item importer

## 0.0.44: 2023-02-24
* Initial Internet Archive import work.

## 0.0.43: 2023-02-23
* Fix ISSNL-ISSN resolution.
* Add only-crossref flag to random-samples.
* Fixes to HathiTrust resolution.

## 0.0.42 (partial): 2023-02-21
* Add literal switch to CLI to forego regex DOI parsing. (Mostly to allow SICI.)
* Add ISSN-L to ISSN translation ability.
* Bugfixes to HathiTrust importer.

## 0.0.42 (partial): 2023-02-20
* Add claim of preservation assertion correctness.

## 0.0.42 (partial): 2023-02-19
* Uniform black coding style.

## 0.0.41: 2023-02-17
* Expansion of unpack_range to allow for different formats.
* Display asserted preservation information from CLI.

## 0.0.40: 2023-02-16
* Add show-issn CLI command.
* Use all ISSNs.

## 0.0.39: 2023-02-16
* Move CLI option to argument.
* Fix resolution of DOIs without certain metadata fields.
* Add CLI logging on resolution.
* Handle non-resolving DOIs via CLI.

## 0.0.38: 2023-02-16
* Inhibit new settings download in every parallel process.

## 0.0.37: 2023-02-16
* Attempt to configure parallel logging.

## 0.0.36: 2023-02-16
* Init parallel environment.

## 0.0.35: 2023-02-16
* Bugfix to parallelization.

## 0.0.34: 2023-02-16
* Add about stamp to generated outputs.
* Parallelize report creation.

## 0.0.33: 2023-02-15
* Add show-archives command.
* Add random-articles command.
* Make random_db_entries atomic to avoid changing object count

## 0.0.32: 2023-02-15
* Remove OTT logging.

## 0.0.31: 2023-02-15
* Added database indexes to speed retrieval.

## 0.0.30: 2023-02-15
* Add debug statements to track down speed problems.

## 0.0.29: 2023-02-15
* Bugfix to journal-article filter.

## 0.0.28: 2023-02-15
* Add caching module to only allow fill once per week.
* Add pytz requirement.
* Add humanfriendly requirement.

## 0.0.27: 2023-02-15
* Move reporting functionality to separate module.

## 0.0.26: 2023-02-14
* HathiTrust: allow multiple ISSNs.

## 0.0.25: 2023-02-14
* Add Hathi debug output.

## 0.0.24: 2023-02-14
* Bugfix for Hathi.

## 0.0.23: 2023-02-14
* Switch Hathi import to streaming method.

## 0.0.22: 2023-02-14
* Bugfix in Portico.

## 0.0.21: 2023-02-14
* Bugfix in Portico.

## 0.0.20: 2023-02-14
* Bugfix in LOCKSS.

## 0.0.19: 2023-02-14
* Trim publisher name if needed.

## 0.0.18: 2023-02-14
* Bufgix to OCUL import.

## 0.0.17: 2023-02-14
* Fix blank title.
* Refactor environment setup.

## 0.0.16: 2023-02-13
* Fix memory leak.

## 0.0.14: 2023-02-13
* Catch ONIX blank values.

## 0.0.14: 2023-02-13
* Add explicit garbage collection to reduce memory usage.

## 0.0.13: 2023-02-13
* Remove logging to attempt to avoid LocalTaskJob heartbeat got an exception error.

## 0.0.12: 2023-02-13
* Remove atomic transactions to reduce memory usage.

## 0.0.11: 2023-02-13
* Bugfix to ONIX parser to handle null publisher.

## 0.0.10: 2023-02-13
* Bugfix to ONIX parser to handle null publisher.

## 0.0.9: 2023-02-13
* Bugfix to ONIX parser.

## 0.0.8: 2023-02-13
* Rewrite ONIX parser to use streaming/event approach to avoid OOM errors.

## 0.0.7: 2023-02-13
* Remove xmltodict dependency.

## 0.0.6: 2023-02-13
* Add OCUL Scholar's Portal import.

## 0.0.5: 2023-02-09
* Move unpack_range function to utils. 

## 0.0.4: 2023-02-09
* Refactor of HathiTrust import.
* Add S3 as data source for download.

## 0.0.3: 2023-02-09
* Refactor of Portico import.
* Refactor of Cariniana import.
* Refactor of LOCKSS Network import.

## 0.0.2: 2023-02-08
* Refactor of PKP Private LOCKSS Network import.

## 0.0.1: 2023-02-06
* First functional version.

This is the first release of preservation-database. It is still very much a work in progress.